##Quick start
Quick start options:

- Clone the repo: git clone https://yagmurkirkagac@bitbucket.org/yagmurkirkagac/spo-assignment.git or download project.

##Terminal Commands
1. Please install maven.
2. Open terminal.
3. Go to project directory (/spo).
4. Run 'mvn spring-boot:run' at terminal.
5. Open postman with POST request to URL: 'localhost:8080/optimization' with application/json object body.
- For example:
```
{
    "rooms": [
        35,
        21,
        17,
        28
    ],
    "senior": 10,
    "junior": 6
}
```

- You will get the response as :

```
[
    {
        "numberOfSeniors": 3,
        "numberOfJuniors": 1
    },
    {
        "numberOfSeniors": 1,
        "numberOfJuniors": 2
    },
    {
        "numberOfSeniors": 2,
        "numberOfJuniors": 0
    },
    {
        "numberOfSeniors": 1,
        "numberOfJuniors": 3
    }
]
```

Thank you