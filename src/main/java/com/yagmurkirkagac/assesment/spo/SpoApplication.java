package com.yagmurkirkagac.assesment.spo;

import com.yagmurkirkagac.assesment.spo.service.IOptimizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpoApplication.class, args);
    }

}
