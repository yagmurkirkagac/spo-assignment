package com.yagmurkirkagac.assesment.spo.model;

import java.util.Objects;

public class WorkforceRto {
    int numberOfSeniors;
    int numberOfJuniors;

    public int getNumberOfSeniors() {
        return numberOfSeniors;
    }

    public void setNumberOfSeniors(int numberOfSeniors) {
        this.numberOfSeniors = numberOfSeniors;
    }

    public int getNumberOfJuniors() {
        return numberOfJuniors;
    }

    public void setNumberOfJuniors(int numberOfJuniors) {
        this.numberOfJuniors = numberOfJuniors;
    }

    public static WorkforceRto map(Workforce workforce) {
        WorkforceRto workforceRto = new WorkforceRto();
        workforceRto.setNumberOfSeniors(workforce.getNumberOfSeniors());
        workforceRto.setNumberOfJuniors(workforce.getNumberOfJuniors());
        return workforceRto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkforceRto that = (WorkforceRto) o;
        return numberOfSeniors == that.numberOfSeniors &&
                numberOfJuniors == that.numberOfJuniors;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfSeniors, numberOfJuniors);
    }

}
