package com.yagmurkirkagac.assesment.spo.model;

public class Workforce {
    int numberOfSeniors;
    int numberOfJuniors;
    int overCapacity;

    public int getNumberOfSeniors() {
        return numberOfSeniors;
    }

    public void setNumberOfSeniors(int numberOfSeniors) {
        this.numberOfSeniors = numberOfSeniors;
    }

    public int getNumberOfJuniors() {
        return numberOfJuniors;
    }

    public void setNumberOfJuniors(int numberOfJuniors) {
        this.numberOfJuniors = numberOfJuniors;
    }

    public int getOverCapacity() {
        return overCapacity;
    }

    public void setOverCapacity(int overCapacity) {
        this.overCapacity = overCapacity;
    }
}
