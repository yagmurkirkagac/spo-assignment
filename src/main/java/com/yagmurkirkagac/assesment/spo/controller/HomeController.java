package com.yagmurkirkagac.assesment.spo.controller;

import com.yagmurkirkagac.assesment.spo.model.WorkforceCapacitiesRto;
import com.yagmurkirkagac.assesment.spo.model.WorkforceRto;
import com.yagmurkirkagac.assesment.spo.service.IOptimizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class HomeController {

    @Autowired
    IOptimizationService iOptimizationService;

    @PostMapping("/optimization")
    public List<WorkforceRto> getResult(@RequestBody WorkforceCapacitiesRto workforceCapacitiesRto) {
        return iOptimizationService.optimizeWorkforce(workforceCapacitiesRto.getRooms(), workforceCapacitiesRto.getSenior(), workforceCapacitiesRto.getJunior());
    }

}
