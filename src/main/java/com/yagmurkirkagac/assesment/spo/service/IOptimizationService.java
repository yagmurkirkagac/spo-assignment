package com.yagmurkirkagac.assesment.spo.service;

import com.yagmurkirkagac.assesment.spo.model.WorkforceRto;

import java.util.List;

public interface IOptimizationService {
    List<WorkforceRto> optimizeWorkforce(int[] rooms, int seniorCapacity, int juniorCapacity);
}
