package com.yagmurkirkagac.assesment.spo.service;

import com.yagmurkirkagac.assesment.spo.model.Workforce;
import com.yagmurkirkagac.assesment.spo.model.WorkforceRto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OptimizationServiceImpl implements IOptimizationService {

    @Override
    public List<WorkforceRto>  optimizeWorkforce(int[] rooms, int seniorCapacity, int juniorCapacity) {

        List<WorkforceRto> result = new ArrayList<>();

        for (int i = 0; i < rooms.length; i++) {
            int roomCapacity = rooms[i];
            roomCapacity -= seniorCapacity;

            int maxSeniorCount = (roomCapacity % seniorCapacity != 0) ? (roomCapacity / seniorCapacity) + 1 : roomCapacity / seniorCapacity;
            int maxJuniorCount = (roomCapacity % juniorCapacity != 0) ? (roomCapacity / juniorCapacity) + 1 : roomCapacity / juniorCapacity;

            List<Workforce> workforceList = new ArrayList<>();
            int min = Integer.MAX_VALUE;

            for (int j = 0; j <= maxSeniorCount; j++) {
                for (int k = 0; k <= maxJuniorCount; k++) {
                    int calculation = (seniorCapacity * j) + (juniorCapacity * k);
                    int overCapacity = calculation - roomCapacity;
                    if (overCapacity >= 0) {
                        Workforce workforce = new Workforce();
                        workforce.setOverCapacity(overCapacity);
                        workforce.setNumberOfJuniors(k);
                        workforce.setNumberOfSeniors(j+1);
                        workforceList.add(workforce);
                        min = overCapacity < min ? overCapacity : min;
                    }
                }
            }
            int minValue = min;
            workforceList = workforceList.stream().filter(w -> w.getOverCapacity() == minValue).collect(Collectors.toList());
            Workforce optimalWorkforce = workforceList.stream().min((w1, w2) -> Integer.compare((w1.getNumberOfSeniors() + w1.getNumberOfJuniors()), (w2.getNumberOfSeniors() + w2.getNumberOfJuniors()))).get();
            result.add(WorkforceRto.map(optimalWorkforce));
        }

        return result;
    }
}
