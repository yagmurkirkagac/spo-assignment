package com.yagmurkirkagac.assesment.spo;

import com.yagmurkirkagac.assesment.spo.model.WorkforceRto;
import com.yagmurkirkagac.assesment.spo.service.IOptimizationService;
import com.yagmurkirkagac.assesment.spo.service.OptimizationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


@RunWith(SpringRunner.class)
public class OptimizationServiceImplTest {

    @InjectMocks
    private OptimizationServiceImpl optimizationService;

    @Test
    public void handleRequest() {
        List<WorkforceRto> handlerResult =optimizationService.optimizeWorkforce(new int []{35}, 10,6);
        WorkforceRto workforceRto=new WorkforceRto();
        workforceRto.setNumberOfJuniors(1);
        workforceRto.setNumberOfSeniors(3);
        List<WorkforceRto> expectedResult= new ArrayList<>();
        expectedResult.add(workforceRto);
        assertEquals(handlerResult,expectedResult);
    }

    @Test
    public void handleRequestFail(){
        List<WorkforceRto> handlerResult =optimizationService.optimizeWorkforce(new int []{35}, 10,6);
        WorkforceRto workforceRto=new WorkforceRto();
        workforceRto.setNumberOfJuniors(4);
        workforceRto.setNumberOfSeniors(3);
        List<WorkforceRto> expectedResult= new ArrayList<>();
        expectedResult.add(workforceRto);
        assertNotEquals(handlerResult,expectedResult);
    }
}
