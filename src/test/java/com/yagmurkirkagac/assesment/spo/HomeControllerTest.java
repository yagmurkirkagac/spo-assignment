package com.yagmurkirkagac.assesment.spo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yagmurkirkagac.assesment.spo.controller.HomeController;
import com.yagmurkirkagac.assesment.spo.model.WorkforceCapacitiesRto;
import com.yagmurkirkagac.assesment.spo.service.OptimizationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(HomeController.class)
public class HomeControllerTest {

    @MockBean
    private OptimizationServiceImpl optimizationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void handlePostRequest() {
        WorkforceCapacitiesRto workforceCapacitiesRto = new WorkforceCapacitiesRto();
        workforceCapacitiesRto.setRooms(new int[]{21});
        workforceCapacitiesRto.setSenior(10);
        workforceCapacitiesRto.setJunior(6);

        try {
            this.mockMvc.perform(post("http://localhost:8080/optimization")
                    .content(mapToJson(workforceCapacitiesRto))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

}
